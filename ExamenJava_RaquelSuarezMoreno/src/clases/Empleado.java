package clases;

/**
 * @author Raquel
 * @date 21.06.2016
 *
 */


// La SuperClase Empleado


public class Empleado
{
	// Sus atributos
	private String nombre;
	private String apellido;
	private String cedula;
	private int edad;
	private boolean casado;
	private double salario;
	
	
	
	
	//Constructor por defecto
	public Empleado()
	{
		this.nombre="R";
		this.apellido="SM";
		this.cedula="a";
		this.edad=18;
		this.casado=false;
		this.salario=0;
	}
	
	//Constructor con par�metros
	public Empleado(String nombre, String apellido, String cedula, int edad, boolean casado, double salario)
	{
		this.nombre=nombre;
		this.apellido=apellido;
		this.cedula=cedula;
		this.edad=edad;
		this.casado=casado;
		this.salario=salario;
	}
	
	
	//M�todo que muestra la clasificaci�n del empleado seg�n la edad
	public void clasificacionSegunEdad(int edad)
	{
		if(edad<=21)
		{
			System.out.println("Principiante");
		}
		else if(edad>=22 && edad<=35)
		{
			System.out.println("Intermedio");
		}
		else
		{
			System.out.println("Senior");
		}
	}
	
	//M�todo que permite aumentar el salario en un porcentaje (�un 10%?)
	public double aumentarSalario(double salario)
	{
		salario=0;
		salario+=salario*0.10;
		return salario;
	}
	
	
	
	
	
	
	//Gets y Sets

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}
	
	public String getCedula() {
		return cedula;
	}

	public int getEdad() {
		return edad;
	}

	public boolean isCasado() {
		return casado;
	}

	public double getSalario() {
		return salario;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public void setCasado(boolean casado) {
		this.casado = casado;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
	
	
}
