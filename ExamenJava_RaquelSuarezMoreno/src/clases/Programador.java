package clases;


/**
 * @author Raquel
 * @date 21.06.2016
 *
 */

// La SubClase Programador 


public class Programador extends Empleado
{
	//Atributos propios de la SubClase
	private int lineasDeCodigoPorHora;
	private String lenguajeDominante;
	
	
	//Constructor por defecto
	public Programador()
	{
		super(); //hereda por defecto todos los atributos de la SuperClase Empleado
		this.lineasDeCodigoPorHora=0;
		this.lenguajeDominante="java";
	}
	
	//Constructor con parámetros
	public Programador(String nombre, String apellido, String cedula, int edad, boolean casado, double salario, int lineasDeCodigoPorHora, String lenguajeDominante)
	{
		super(nombre, apellido, cedula, edad, casado, salario); //hereda con parámetros todos los atributos de la SuperClase Empleado
		this.lineasDeCodigoPorHora=lineasDeCodigoPorHora;
		this.lenguajeDominante=lenguajeDominante;
	}
	
	
	
	
	
	//Gets y Sets

	public int getLineasDeCodigoPorHora() {
		return lineasDeCodigoPorHora;
	}

	public String getLenguajeDominante() {
		return lenguajeDominante;
	}

	public void setLineasDeCodigoPorHora(int lineasDeCodigoPorHora) {
		this.lineasDeCodigoPorHora = lineasDeCodigoPorHora;
	}

	public void setLenguajeDominante(String lenguajeDominante) {
		this.lenguajeDominante = lenguajeDominante;
	}
	
	
	
}











