/**
 * 
 */
package main;

import clases.Empleado;
import clases.Programador;

/**
 * @author Raquel
 * @date 21.06.2016
 *
 */
public class Main
{

	public static void main(String[] args)
	{
		//Objetos
		Empleado trabajador1 = new Empleado("Raquel", "Suarez", "f", 24, false, 800); //con par�metros
		Empleado trabajador2 = new Empleado(); //por defecto
		Programador trabajador3 = new Programador(); //por defecto
		Programador trabajador4 = new Programador("Raquel", "Suarez", "c", 24, false, 600, 500, "C++"); //con par�metros
		
		
	
	System.out.print("\n Los datos del trabajador 1 son: \n Nombre: " +trabajador1.getNombre()+
			"\n Apellido: " +trabajador1.getApellido()+ "\n Cedula: " +trabajador1.getCedula()+
			"\n Edad: " +trabajador1.getEdad()+ "\n �Est� casado? " +trabajador1.isCasado()+
			"\n Salario: " +trabajador1.getSalario());
	System.out.print("\n");
	System.out.print("\n Los datos del trabajador 2 son: \n Nombre: " +trabajador2.getNombre()+
			"\n Apellido: " +trabajador2.getApellido()+ "\n Cedula: " +trabajador2.getCedula()+
			"\n Edad: " +trabajador2.getEdad()+ "\n �Est� casado? " +trabajador2.isCasado()+
			"\n Salario: " +trabajador2.getSalario());
	System.out.print("\n");
	System.out.print("\n Los datos del trabajador (que es programador) 3 son: \n Nombre: " +trabajador3.getNombre()+
			"\n Apellido: " +trabajador3.getApellido()+ "\n Cedula: " +trabajador3.getCedula()+
			"\n Edad: " +trabajador3.getEdad()+ "\n �Est� casado? " +trabajador3.isCasado()+
			"\n Salario: " +trabajador3.getSalario()+ "\n L�neas de c�digo: " +trabajador3.getLineasDeCodigoPorHora()+
			"\n Lenguaje: " +trabajador3.getLenguajeDominante());
	System.out.print("\n");
	System.out.print("\n Los datos del trabajador (que es programador) 4 son: \n Nombre: " +trabajador4.getNombre()+
			"\n Apellido: " +trabajador4.getApellido()+ "\n Cedula: " +trabajador4.getCedula()+
			"\n Edad: " +trabajador4.getEdad()+ "\n �Est� casado? " +trabajador4.isCasado()+
			"\n Salario: " +trabajador4.getSalario()+ "\n L�neas de c�digo: " +trabajador4.getLineasDeCodigoPorHora()+
			"\n Lenguaje: " +trabajador4.getLenguajeDominante());
	
	}

}
